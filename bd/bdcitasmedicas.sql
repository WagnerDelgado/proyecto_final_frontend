PGDMP                          z            bdcitasmedicas    10.16    10.16 F    P           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                       false            Q           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                       false            R           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                       false            S           1262    77829    bdcitasmedicas    DATABASE     �   CREATE DATABASE bdcitasmedicas WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'English_United States.1252' LC_CTYPE = 'English_United States.1252';
    DROP DATABASE bdcitasmedicas;
             postgres    false                        2615    2200    public    SCHEMA        CREATE SCHEMA public;
    DROP SCHEMA public;
             postgres    false            T           0    0    SCHEMA public    COMMENT     6   COMMENT ON SCHEMA public IS 'standard public schema';
                  postgres    false    3                        3079    12924    plpgsql 	   EXTENSION     ?   CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;
    DROP EXTENSION plpgsql;
                  false            U           0    0    EXTENSION plpgsql    COMMENT     @   COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';
                       false    1            �            1259    86207    cita    TABLE     �   CREATE TABLE public.cita (
    id_cita integer NOT NULL,
    fecha_cita timestamp without time zone NOT NULL,
    hora_cita character varying(255) NOT NULL,
    id_medico integer NOT NULL,
    id_paciente integer NOT NULL
);
    DROP TABLE public.cita;
       public         postgres    false    3            �            1259    86205    cita_id_cita_seq    SEQUENCE     �   CREATE SEQUENCE public.cita_id_cita_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 '   DROP SEQUENCE public.cita_id_cita_seq;
       public       postgres    false    3    210            V           0    0    cita_id_cita_seq    SEQUENCE OWNED BY     E   ALTER SEQUENCE public.cita_id_cita_seq OWNED BY public.cita.id_cita;
            public       postgres    false    209            �            1259    77840    especialidad    TABLE     �   CREATE TABLE public.especialidad (
    id_especialidad integer NOT NULL,
    descripcion character varying(100) NOT NULL,
    nombre character varying(100) NOT NULL
);
     DROP TABLE public.especialidad;
       public         postgres    false    3            �            1259    77838     especialidad_id_especialidad_seq    SEQUENCE     �   CREATE SEQUENCE public.especialidad_id_especialidad_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 7   DROP SEQUENCE public.especialidad_id_especialidad_seq;
       public       postgres    false    3    197            W           0    0     especialidad_id_especialidad_seq    SEQUENCE OWNED BY     e   ALTER SEQUENCE public.especialidad_id_especialidad_seq OWNED BY public.especialidad.id_especialidad;
            public       postgres    false    196            �            1259    77848    horario    TABLE     �   CREATE TABLE public.horario (
    id_horario integer NOT NULL,
    dias character varying(255) NOT NULL,
    hora_fin character varying(255) NOT NULL,
    hora_inicio character varying(255) NOT NULL
);
    DROP TABLE public.horario;
       public         postgres    false    3            �            1259    77846    horario_id_horario_seq    SEQUENCE     �   CREATE SEQUENCE public.horario_id_horario_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 -   DROP SEQUENCE public.horario_id_horario_seq;
       public       postgres    false    199    3            X           0    0    horario_id_horario_seq    SEQUENCE OWNED BY     Q   ALTER SEQUENCE public.horario_id_horario_seq OWNED BY public.horario.id_horario;
            public       postgres    false    198            �            1259    86182    medico    TABLE       CREATE TABLE public.medico (
    id_medico integer NOT NULL,
    apellidos character varying(100) NOT NULL,
    cedula character varying(13) NOT NULL,
    nombres character varying(100) NOT NULL,
    id_especialidad integer NOT NULL,
    id_horario integer NOT NULL
);
    DROP TABLE public.medico;
       public         postgres    false    3            �            1259    86180    medico_id_medico_seq    SEQUENCE     �   CREATE SEQUENCE public.medico_id_medico_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 +   DROP SEQUENCE public.medico_id_medico_seq;
       public       postgres    false    208    3            Y           0    0    medico_id_medico_seq    SEQUENCE OWNED BY     M   ALTER SEQUENCE public.medico_id_medico_seq OWNED BY public.medico.id_medico;
            public       postgres    false    207            �            1259    77865    menu    TABLE     �   CREATE TABLE public.menu (
    id_menu integer NOT NULL,
    icono character varying(20),
    nombre character varying(20),
    url character varying(50)
);
    DROP TABLE public.menu;
       public         postgres    false    3            �            1259    77870    menu_rol    TABLE     \   CREATE TABLE public.menu_rol (
    id_menu integer NOT NULL,
    id_rol integer NOT NULL
);
    DROP TABLE public.menu_rol;
       public         postgres    false    3            �            1259    86164    paciente    TABLE     |  CREATE TABLE public.paciente (
    id_paciente integer NOT NULL,
    apellidos character varying(100) NOT NULL,
    cedula character varying(13) NOT NULL,
    nombres character varying(100) NOT NULL,
    password character varying(100) NOT NULL,
    sexo character varying(100) NOT NULL,
    telefono character varying(15) NOT NULL,
    usuario character varying(100) NOT NULL
);
    DROP TABLE public.paciente;
       public         postgres    false    3            �            1259    86162    paciente_id_paciente_seq    SEQUENCE     �   CREATE SEQUENCE public.paciente_id_paciente_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 /   DROP SEQUENCE public.paciente_id_paciente_seq;
       public       postgres    false    206    3            Z           0    0    paciente_id_paciente_seq    SEQUENCE OWNED BY     U   ALTER SEQUENCE public.paciente_id_paciente_seq OWNED BY public.paciente.id_paciente;
            public       postgres    false    205            �            1259    77884    rol    TABLE     �   CREATE TABLE public.rol (
    id_rol integer NOT NULL,
    descripcion character varying(150),
    nombre character varying(50) NOT NULL
);
    DROP TABLE public.rol;
       public         postgres    false    3            �            1259    77889    usuario    TABLE     �   CREATE TABLE public.usuario (
    id_usuario integer NOT NULL,
    estado boolean NOT NULL,
    clave character varying(255) NOT NULL,
    nombre character varying(255) NOT NULL
);
    DROP TABLE public.usuario;
       public         postgres    false    3            �            1259    77897    usuario_rol    TABLE     b   CREATE TABLE public.usuario_rol (
    id_usuario integer NOT NULL,
    id_rol integer NOT NULL
);
    DROP TABLE public.usuario_rol;
       public         postgres    false    3            �
           2604    86210    cita id_cita    DEFAULT     l   ALTER TABLE ONLY public.cita ALTER COLUMN id_cita SET DEFAULT nextval('public.cita_id_cita_seq'::regclass);
 ;   ALTER TABLE public.cita ALTER COLUMN id_cita DROP DEFAULT;
       public       postgres    false    209    210    210            �
           2604    77843    especialidad id_especialidad    DEFAULT     �   ALTER TABLE ONLY public.especialidad ALTER COLUMN id_especialidad SET DEFAULT nextval('public.especialidad_id_especialidad_seq'::regclass);
 K   ALTER TABLE public.especialidad ALTER COLUMN id_especialidad DROP DEFAULT;
       public       postgres    false    196    197    197            �
           2604    77851    horario id_horario    DEFAULT     x   ALTER TABLE ONLY public.horario ALTER COLUMN id_horario SET DEFAULT nextval('public.horario_id_horario_seq'::regclass);
 A   ALTER TABLE public.horario ALTER COLUMN id_horario DROP DEFAULT;
       public       postgres    false    198    199    199            �
           2604    86185    medico id_medico    DEFAULT     t   ALTER TABLE ONLY public.medico ALTER COLUMN id_medico SET DEFAULT nextval('public.medico_id_medico_seq'::regclass);
 ?   ALTER TABLE public.medico ALTER COLUMN id_medico DROP DEFAULT;
       public       postgres    false    207    208    208            �
           2604    86167    paciente id_paciente    DEFAULT     |   ALTER TABLE ONLY public.paciente ALTER COLUMN id_paciente SET DEFAULT nextval('public.paciente_id_paciente_seq'::regclass);
 C   ALTER TABLE public.paciente ALTER COLUMN id_paciente DROP DEFAULT;
       public       postgres    false    206    205    206            M          0    86207    cita 
   TABLE DATA               V   COPY public.cita (id_cita, fecha_cita, hora_cita, id_medico, id_paciente) FROM stdin;
    public       postgres    false    210   �O       @          0    77840    especialidad 
   TABLE DATA               L   COPY public.especialidad (id_especialidad, descripcion, nombre) FROM stdin;
    public       postgres    false    197   P       B          0    77848    horario 
   TABLE DATA               J   COPY public.horario (id_horario, dias, hora_fin, hora_inicio) FROM stdin;
    public       postgres    false    199   [P       K          0    86182    medico 
   TABLE DATA               d   COPY public.medico (id_medico, apellidos, cedula, nombres, id_especialidad, id_horario) FROM stdin;
    public       postgres    false    208   �P       C          0    77865    menu 
   TABLE DATA               ;   COPY public.menu (id_menu, icono, nombre, url) FROM stdin;
    public       postgres    false    200   Q       D          0    77870    menu_rol 
   TABLE DATA               3   COPY public.menu_rol (id_menu, id_rol) FROM stdin;
    public       postgres    false    201   �Q       I          0    86164    paciente 
   TABLE DATA               n   COPY public.paciente (id_paciente, apellidos, cedula, nombres, password, sexo, telefono, usuario) FROM stdin;
    public       postgres    false    206   �Q       E          0    77884    rol 
   TABLE DATA               :   COPY public.rol (id_rol, descripcion, nombre) FROM stdin;
    public       postgres    false    202   UR       F          0    77889    usuario 
   TABLE DATA               D   COPY public.usuario (id_usuario, estado, clave, nombre) FROM stdin;
    public       postgres    false    203   �R       G          0    77897    usuario_rol 
   TABLE DATA               9   COPY public.usuario_rol (id_usuario, id_rol) FROM stdin;
    public       postgres    false    204   �S       [           0    0    cita_id_cita_seq    SEQUENCE SET     ?   SELECT pg_catalog.setval('public.cita_id_cita_seq', 16, true);
            public       postgres    false    209            \           0    0     especialidad_id_especialidad_seq    SEQUENCE SET     N   SELECT pg_catalog.setval('public.especialidad_id_especialidad_seq', 8, true);
            public       postgres    false    196            ]           0    0    horario_id_horario_seq    SEQUENCE SET     D   SELECT pg_catalog.setval('public.horario_id_horario_seq', 5, true);
            public       postgres    false    198            ^           0    0    medico_id_medico_seq    SEQUENCE SET     C   SELECT pg_catalog.setval('public.medico_id_medico_seq', 11, true);
            public       postgres    false    207            _           0    0    paciente_id_paciente_seq    SEQUENCE SET     F   SELECT pg_catalog.setval('public.paciente_id_paciente_seq', 8, true);
            public       postgres    false    205            �
           2606    86212    cita cita_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.cita
    ADD CONSTRAINT cita_pkey PRIMARY KEY (id_cita);
 8   ALTER TABLE ONLY public.cita DROP CONSTRAINT cita_pkey;
       public         postgres    false    210            �
           2606    77845    especialidad especialidad_pkey 
   CONSTRAINT     i   ALTER TABLE ONLY public.especialidad
    ADD CONSTRAINT especialidad_pkey PRIMARY KEY (id_especialidad);
 H   ALTER TABLE ONLY public.especialidad DROP CONSTRAINT especialidad_pkey;
       public         postgres    false    197            �
           2606    77856    horario horario_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.horario
    ADD CONSTRAINT horario_pkey PRIMARY KEY (id_horario);
 >   ALTER TABLE ONLY public.horario DROP CONSTRAINT horario_pkey;
       public         postgres    false    199            �
           2606    86187    medico medico_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.medico
    ADD CONSTRAINT medico_pkey PRIMARY KEY (id_medico);
 <   ALTER TABLE ONLY public.medico DROP CONSTRAINT medico_pkey;
       public         postgres    false    208            �
           2606    77869    menu menu_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.menu
    ADD CONSTRAINT menu_pkey PRIMARY KEY (id_menu);
 8   ALTER TABLE ONLY public.menu DROP CONSTRAINT menu_pkey;
       public         postgres    false    200            �
           2606    86172    paciente paciente_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.paciente
    ADD CONSTRAINT paciente_pkey PRIMARY KEY (id_paciente);
 @   ALTER TABLE ONLY public.paciente DROP CONSTRAINT paciente_pkey;
       public         postgres    false    206            �
           2606    77888    rol rol_pkey 
   CONSTRAINT     N   ALTER TABLE ONLY public.rol
    ADD CONSTRAINT rol_pkey PRIMARY KEY (id_rol);
 6   ALTER TABLE ONLY public.rol DROP CONSTRAINT rol_pkey;
       public         postgres    false    202            �
           2606    86214 !   cita uk_b6fb9k0uu6p52leldtffgsy5e 
   CONSTRAINT     a   ALTER TABLE ONLY public.cita
    ADD CONSTRAINT uk_b6fb9k0uu6p52leldtffgsy5e UNIQUE (hora_cita);
 K   ALTER TABLE ONLY public.cita DROP CONSTRAINT uk_b6fb9k0uu6p52leldtffgsy5e;
       public         postgres    false    210            �
           2606    86189 #   medico uk_bwjkgltyvx3gdko86uxv66s1s 
   CONSTRAINT     `   ALTER TABLE ONLY public.medico
    ADD CONSTRAINT uk_bwjkgltyvx3gdko86uxv66s1s UNIQUE (cedula);
 M   ALTER TABLE ONLY public.medico DROP CONSTRAINT uk_bwjkgltyvx3gdko86uxv66s1s;
       public         postgres    false    208            �
           2606    77911 $   usuario uk_cto7dkti4t38iq8r4cqesbd8k 
   CONSTRAINT     a   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT uk_cto7dkti4t38iq8r4cqesbd8k UNIQUE (nombre);
 N   ALTER TABLE ONLY public.usuario DROP CONSTRAINT uk_cto7dkti4t38iq8r4cqesbd8k;
       public         postgres    false    203            �
           2606    77905 $   horario uk_rl29hk2wlow4lv1lcfg44a8w8 
   CONSTRAINT     _   ALTER TABLE ONLY public.horario
    ADD CONSTRAINT uk_rl29hk2wlow4lv1lcfg44a8w8 UNIQUE (dias);
 N   ALTER TABLE ONLY public.horario DROP CONSTRAINT uk_rl29hk2wlow4lv1lcfg44a8w8;
       public         postgres    false    199            �
           2606    86174 $   paciente uk_rmrfxh63vnjlbufy2w0nig6e 
   CONSTRAINT     a   ALTER TABLE ONLY public.paciente
    ADD CONSTRAINT uk_rmrfxh63vnjlbufy2w0nig6e UNIQUE (cedula);
 N   ALTER TABLE ONLY public.paciente DROP CONSTRAINT uk_rmrfxh63vnjlbufy2w0nig6e;
       public         postgres    false    206            �
           2606    77903 )   especialidad uk_t1d8l1jfavmv5l7rf1ub9pt20 
   CONSTRAINT     f   ALTER TABLE ONLY public.especialidad
    ADD CONSTRAINT uk_t1d8l1jfavmv5l7rf1ub9pt20 UNIQUE (nombre);
 S   ALTER TABLE ONLY public.especialidad DROP CONSTRAINT uk_t1d8l1jfavmv5l7rf1ub9pt20;
       public         postgres    false    197            �
           2606    77896    usuario usuario_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.usuario
    ADD CONSTRAINT usuario_pkey PRIMARY KEY (id_usuario);
 >   ALTER TABLE ONLY public.usuario DROP CONSTRAINT usuario_pkey;
       public         postgres    false    203            �
           2606    77947 '   usuario_rol fk3ftpt75ebughsiy5g03b11akt    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT fk3ftpt75ebughsiy5g03b11akt FOREIGN KEY (id_usuario) REFERENCES public.usuario(id_usuario);
 Q   ALTER TABLE ONLY public.usuario_rol DROP CONSTRAINT fk3ftpt75ebughsiy5g03b11akt;
       public       postgres    false    204    2737    203            �
           2606    77937 $   menu_rol fk8uyjomykqlidw6whr5a9vx16d    FK CONSTRAINT     �   ALTER TABLE ONLY public.menu_rol
    ADD CONSTRAINT fk8uyjomykqlidw6whr5a9vx16d FOREIGN KEY (id_menu) REFERENCES public.menu(id_menu);
 N   ALTER TABLE ONLY public.menu_rol DROP CONSTRAINT fk8uyjomykqlidw6whr5a9vx16d;
       public       postgres    false    2731    200    201            �
           2606    86215    cita fk_cita_medico    FK CONSTRAINT     |   ALTER TABLE ONLY public.cita
    ADD CONSTRAINT fk_cita_medico FOREIGN KEY (id_medico) REFERENCES public.medico(id_medico);
 =   ALTER TABLE ONLY public.cita DROP CONSTRAINT fk_cita_medico;
       public       postgres    false    208    210    2743            �
           2606    86220    cita fk_cita_paciente    FK CONSTRAINT     �   ALTER TABLE ONLY public.cita
    ADD CONSTRAINT fk_cita_paciente FOREIGN KEY (id_paciente) REFERENCES public.paciente(id_paciente);
 ?   ALTER TABLE ONLY public.cita DROP CONSTRAINT fk_cita_paciente;
       public       postgres    false    2739    206    210            �
           2606    86195    medico fk_medico_especialidad    FK CONSTRAINT     �   ALTER TABLE ONLY public.medico
    ADD CONSTRAINT fk_medico_especialidad FOREIGN KEY (id_especialidad) REFERENCES public.especialidad(id_especialidad);
 G   ALTER TABLE ONLY public.medico DROP CONSTRAINT fk_medico_especialidad;
       public       postgres    false    2723    208    197            �
           2606    86200    medico fk_medico_horario    FK CONSTRAINT     �   ALTER TABLE ONLY public.medico
    ADD CONSTRAINT fk_medico_horario FOREIGN KEY (id_horario) REFERENCES public.horario(id_horario);
 B   ALTER TABLE ONLY public.medico DROP CONSTRAINT fk_medico_horario;
       public       postgres    false    2727    199    208            �
           2606    77932 $   menu_rol fkjtnyb2364scc8ojb7vwh2jflj    FK CONSTRAINT     �   ALTER TABLE ONLY public.menu_rol
    ADD CONSTRAINT fkjtnyb2364scc8ojb7vwh2jflj FOREIGN KEY (id_rol) REFERENCES public.rol(id_rol);
 N   ALTER TABLE ONLY public.menu_rol DROP CONSTRAINT fkjtnyb2364scc8ojb7vwh2jflj;
       public       postgres    false    202    2733    201            �
           2606    77942 '   usuario_rol fkkxcv7htfnm9x1wkofnud0ewql    FK CONSTRAINT     �   ALTER TABLE ONLY public.usuario_rol
    ADD CONSTRAINT fkkxcv7htfnm9x1wkofnud0ewql FOREIGN KEY (id_rol) REFERENCES public.rol(id_rol);
 Q   ALTER TABLE ONLY public.usuario_rol DROP CONSTRAINT fkkxcv7htfnm9x1wkofnud0ewql;
       public       postgres    false    204    2733    202            M   0   x�34�4202�50�56T00�20 "NC3+c� _NCN�=... ��6      @   ?   x�3�t�,*�J�ˇ3��9��Jsr�s����\&�!�E���
�y%�yře��"\1z\\\ kW@      B   G   x�3��	�s�4��20P��44�21��8}�B�Js�z�9��`�5��
u�*e�Ԉݾ=... ��      K   J   x�34��qt�w�4�00243��4�t	�S�pu��4�4�24�tqq���07351�
uurT K��qqq �      C   �   x�e�;�@Dk�0D�N�((�8 2^���d��)�=!�@K7��i�����4@�����1i�@ސ��[�0:�h
�$�W�������8<�l	�k�_(�HT�qG�x�E6�����'q7�~nt@Hm��u��o-�N�      D       x�3�4�2bc 6bS0��7����� CT�      I   t   x�-˽
�0���#��5/%���@��:Z� 8��
v>�"7���c�.���:3�X�1��_�I+�)��Y�x,����I*W���a����Yeۇ<d�r�o,k{��wD�Se      E   @   x�3���Qpt����	rt����2.�
��.������&����	��b���� 
f�      F   �   x�E�Kv�0  �59�kb@�!*U�)��M� �RPN��9� aV]!i�G9���m�3���,q���a,�Dގj����>QL��®t�l�@�<��:�Z;�!.-���B|�n�����9�Y��m��KBQ��F�ȟ���PҝY��^�ώ��Yz\�/K=���A�̧�/��}2B7Ѫb`�y$�M��H�9���x%��5�z��G�z���)���3�=h3��� �7-7[      G      x�3�4�2�4�2�4����� A     