



//#region PATH DASHBOARD
const arquitecturaPage = 'dashboard';
const arquitecturaOfficePage = 'office';
const arquitecturaDivisionPage = 'division';
const arquitecturaPositionPage = 'position';
const arquitecturaEmployeePage = 'employee';

export const PATHS_DASHBOARD_PAGES = {
	withSlash: `/${arquitecturaPage}`,
	onlyPath: arquitecturaPage,

	officePage: {
		withSlash: `/${arquitecturaPage}/${arquitecturaOfficePage}`,
		onlyPath: arquitecturaOfficePage
	},
	divisionPage: {
		withSlash: `/${arquitecturaPage}/${arquitecturaDivisionPage}`,
		onlyPath: arquitecturaDivisionPage
	},
	positionPage: {
		withSlash: `/${arquitecturaPage}/${arquitecturaPositionPage}`,
		onlyPath: arquitecturaPositionPage
	},
  employeePage: {
		withSlash: `/${arquitecturaPage}/${arquitecturaEmployeePage}`,
		onlyPath: arquitecturaEmployeePage
	}
};

//#endregion
