import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { ListOfficeComponent } from '../arquitectura/office/list-office/list-office.component';
import { AddEditOfficeComponent } from '../arquitectura/office/add-edit-office/add-edit-office.component';
import { ListDivisionComponent } from '../arquitectura/division/list-division/list-division.component';
import { AddEditDivisionComponent } from '../arquitectura/division/add-edit-division/add-edit-division.component';
import { AddEditPositionComponent } from '../arquitectura/position/add-edit-position/add-edit-position.component';
import { ListPositionComponent } from '../arquitectura/position/list-position/list-position.component';
import { NavbarComponent } from '../arquitectura/navbar/navbar.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MaterialModule } from '../material/material.module';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { ListSalaryComponent } from '../arquitectura/salary/list-salary/list-salary.component';
import { AddEditSalaryComponent } from '../arquitectura/salary/add-edit-salary/add-edit-salary.component';
import { ListEmployeeSalaryComponent } from '../arquitectura/employeeSalary/list-employee-salary/list-employee-salary.component';
import { AddEmployeeSalaryComponent } from '../arquitectura/employeeSalary/add-employee-salary/add-employee-salary.component';
import { AddListEmployeeSalaryComponent } from '../arquitectura/employeeSalary/add-list-employee-salary/add-list-employee-salary.component';
import { ListEspecialidadComponent } from '../arquitectura/especialidad/list-especialidad/list-especialidad.component';
import { AddEditEspecialidadComponent } from '../arquitectura/especialidad/add-edit-especialidad/add-edit-especialidad.component';
import { ListMedicoComponent } from '../arquitectura/medico/list-medico/list-medico.component';
import { AddEditMedicoComponent } from '../arquitectura/medico/add-edit-medico/add-edit-medico.component';
import { DetalleFotoComponent } from '../arquitectura/medico/detalle-foto/detalle-foto.component';
import { MedicoDialogoComponent } from '../arquitectura/medico/medico-dialogo/medico-dialogo.component';
import { MedicoEspecialComponent } from '../arquitectura/medico/medico-especial/medico-especial.component';
import { ListPacienteComponent } from '../arquitectura/paciente/list-paciente/list-paciente.component';
import { AddEditPacienteComponent } from '../arquitectura/paciente/add-edit-paciente/add-edit-paciente.component';
import { ListCitasComponent } from '../arquitectura/citas/list-citas/list-citas.component';
import { AddEditCitasComponent } from '../arquitectura/citas/add-edit-citas/add-edit-citas.component';
import { MatNativeDateModule, MAT_DATE_FORMATS, MAT_DATE_LOCALE } from '@angular/material/core';
import { MomentDateModule } from '@angular/material-moment-adapter';
import { ArquitecturaRoutingModule } from './arquitectura-routing.module';
import { LayoutComponent } from './layout/layout.component';

export const MY_DATE_FORMATS = {
  parse: {
    dateInput: 'DD/MM/YYYY',
  },
  display: {
    dateInput: 'DD/MM/YYYY',
    monthYearLabel: 'MMMM YYYY',
    dateA11yLabel: 'LL',
    monthYearA11yLabel: 'MMMM YYYY'
  },
};

@NgModule({
  imports: [
    MaterialModule,
    CommonModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    ArquitecturaRoutingModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MomentDateModule

  ],
  exports: [
    MatNativeDateModule
  ],
  declarations: [
    ListOfficeComponent,
    AddEditOfficeComponent,
    ListDivisionComponent,
    AddEditDivisionComponent,
    AddEditPositionComponent,
    ListPositionComponent,
    NavbarComponent,
    ListSalaryComponent,
    AddEditSalaryComponent,
    ListEmployeeSalaryComponent,
    AddEmployeeSalaryComponent,
    AddListEmployeeSalaryComponent,
    ListEspecialidadComponent,
    AddEditEspecialidadComponent,
    ListMedicoComponent,
    AddEditMedicoComponent,
    DetalleFotoComponent,
    MedicoDialogoComponent,
    MedicoEspecialComponent,
    ListPacienteComponent,
    AddEditPacienteComponent,
    ListCitasComponent,
    AddEditCitasComponent,
    LayoutComponent

  ],
  providers: [
    { provide: MAT_DATE_FORMATS, useValue: MY_DATE_FORMATS }
  ],

})
export class ArquitecturaModule { }
