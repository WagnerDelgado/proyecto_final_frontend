import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { OfficeModel } from '../../models/office';
import { OfficeService } from '../../services/office.service';

@Component({
  selector: 'app-list-office',
  templateUrl: './list-office.component.html',
  styleUrls: ['./list-office.component.scss']
})
export class ListOfficeComponent implements OnInit {

  officeModel: OfficeModel;
  officeArray: OfficeModel[] = [];
  displayedColumns: string[] = ["id", "name", "acciones"];
  dataSource = new MatTableDataSource();
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private _router: Router, private _service: OfficeService) {
    this.CargarTabla();
  }

  Buscar(filtrar: string): void {
    this.dataSource.filter = filtrar.trim().toLowerCase();
  }

  CargarTabla() {
    this.dataSource = new MatTableDataSource();
    this.isLoading = true;
    this._service.getOffice().subscribe(
      (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      },
      (error) => (this.isLoading = false)
    );
  }

  ngOnInit(): void {}

  Eliminar(office: OfficeModel): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Esta seguro?',
        text: 'Seguro que desea eliminar la Oficina?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this._service.deleteOffice(office.id).subscribe(
            (res) => {

                this.officeArray = this.officeArray.filter((office) => office !== office);
                this.CargarTabla();
                swalWithBootstrapButtons.fire(
                  'Eliminado!',
                  `La oficina ha sido eliminado!`,
                  'success'
                );

            },
            (error) => {
              swalWithBootstrapButtons.fire(
                'No se pudo Eliminar!',
                `La oficina ya se ha generado anteriormente!`,
                'warning'
              );
            }
          );
        }
      });
  }

}
