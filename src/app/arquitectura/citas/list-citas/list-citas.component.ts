import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CitaMedicaModel } from '../../models/citaMedica';
import { CitaService } from '../../services/cita.service';

@Component({
  selector: 'app-list-citas',
  templateUrl: './list-citas.component.html',
  styleUrls: ['./list-citas.component.scss']
})
export class ListCitasComponent implements OnInit {

  citasModel: CitaMedicaModel;
  citaArray: CitaMedicaModel[] = [];
  displayedColumns: string[] = ["id_cita", "fecha_cita", "hora_cita", "id_medico", "id_paciente", "acciones"];
  dataSource = new MatTableDataSource();
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private _router: Router, private _service: CitaService) {
    this.CargarTabla();
  }

  Buscar(filtrar: string): void {
    this.dataSource.filter = filtrar.trim().toLowerCase();
  }

  CargarTabla() {
    this.dataSource = new MatTableDataSource();
    this.isLoading = true;
    this._service.getCitas().subscribe(
      (res: any) => {
        console.log(res);
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      },
      (error) => (this.isLoading = false)
    );
  }


  ngOnInit(): void {}

  Eliminar(cita: CitaMedicaModel): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Esta seguro?',
        text: 'Seguro que desea eliminar la Cita?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          console.log(cita.id_cita);
          this._service.deleteCitas(cita.id_cita).subscribe(
            (res) => {

                this.citaArray = this.citaArray.filter((cita) => cita !== cita);
                this.CargarTabla();
                swalWithBootstrapButtons.fire(
                  'Eliminado!',
                  `La cita ha sido eliminada!`,
                  'success'
                );

            },
            (error) => {
              swalWithBootstrapButtons.fire(
                'No se pudo Eliminar!',
                `La cita se ha generado anteriormente!`,
                'warning'
              );
            }
          );
        }
      });
  }
}
