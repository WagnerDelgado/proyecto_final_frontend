import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { CitaMedicaModel } from '../../models/citaMedica';
import { MedicoModel } from '../../models/medico';
import { PacienteModel } from '../../models/paciente';
import { CitaService } from '../../services/cita.service';

@Component({
  selector: 'app-add-edit-citas',
  templateUrl: './add-edit-citas.component.html',
  styleUrls: ['./add-edit-citas.component.scss']
})
export class AddEditCitasComponent implements OnInit {


  constructor(private _service: CitaService, private _router: Router, private _activatedRoute: ActivatedRoute, private _fb: FormBuilder) {
    this.CitaMedicaForm();
  }


  citaModel: CitaMedicaModel=new CitaMedicaModel();
  citasArray: CitaMedicaModel[]=[];
  medico: MedicoModel = new MedicoModel();
  medicoArray: MedicoModel[];
  paciente: PacienteModel = new PacienteModel();
  pacienteArray: PacienteModel[];

  horaCita = ["15:30 PM","16:30 PM","17:30 PM","18:30 PM","19:00 PM"];

  cargarCitas(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["id_cita"];
      if (id) {
        this._service.getCitasById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.citaModel = res;
            this.citaModel.medico = res.medico;
            this.CitaMedicaForm(this.citaModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  createCitaMedicaForm: FormGroup;

  CitaMedicaForm(cita: CitaMedicaModel = null) {
    this.createCitaMedicaForm = this._fb.group({
      id_cita: [cita !== null ? cita.id_cita : 0],
      fecha_cita: [cita !== null ? cita.fecha_cita : "", Validators.required],
      hora_cita: [cita !== null ? cita.hora_cita : "", Validators.required],
      medico: [cita !== null ? cita.medico : null],
      paciente: [cita !== null ? cita.paciente : null]
    });
  }


  ngOnInit(): void {
    this.cargarCitas();
    this._service.getMedicos().subscribe((response) => {
      this.medicoArray = response;
      console.log(response);
    });

    this._service.getPacientes().subscribe((response) => {
      this.pacienteArray = response;
      console.log(response);
    });
  }

  Guardar(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `Se va a generar una cita medica!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.citaModel = this.createCitaMedicaForm.value;
          console.log(this.citaModel);
          if (this.citaModel.id_cita == 0) {
            if (this.createCitaMedicaForm.valid) {
                    this._service.postCitas(this.citaModel).subscribe((res) => {
                      console.log(res);
                        this.cargarCitas();
                        swalWithBootstrapButtons.fire("Guardado!", `La Cita, ha sido generada`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `La Cita`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  Modificar(): void {

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
        },
        buttonsStyling: false,
    });

    swalWithBootstrapButtons
        .fire({
        title: "Modificar!",
        text: `Desea modificar la Cita?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
        })
        .then((result) => {
        if (result.isConfirmed) {
            this.citaModel = this.createCitaMedicaForm.value;
            if (this.createCitaMedicaForm.valid) {
                this._service.putCitas(this.citaModel).subscribe((res) => {
                if(res){
                //this.medicoArray = this.medicoArray.filter((medico) => medico !== this.medico);
                this.cargarCitas();
                swalWithBootstrapButtons.fire(
                    "Modificado!",
                    `La Cita, ha sido modificado`,
                    "success"
                );
                this.Regresar();
                }else{
                swalWithBootstrapButtons.fire(
                    "No se pudo Modificar!",
                    `La Cita, ya existe y/o se encuentra vinculado!`,
                    "warning"
                );
                }
            });
        }else{
            this.Regresar();
        }

            }
        });
    }

  Regresar(): void {
    this._router.navigate(["/arquitectura/list-citas"]);
  }

}
