import { Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { PacienteModel } from '../../models/paciente';
import { PacienteService } from '../../services/paciente.service';

@Component({
  selector: 'app-add-edit-paciente',
  templateUrl: './add-edit-paciente.component.html',
  styleUrls: ['./add-edit-paciente.component.scss']
})
export class AddEditPacienteComponent implements OnInit {
  pacienteModel: PacienteModel = new PacienteModel();
  pacienteArray: PacienteModel []=[];
  especialidadSeleccionada: number;
  sexoList = ['MASCULINO', 'FEMENINO'];

  constructor(private _router: Router, private _service: PacienteService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.PacienteForm();
  }

  ngOnInit(): void {
    this.cargarPaciente();
  }

  cargarPaciente(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["idPaciente"];
      if (id) {
        this._service.getPacienteById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.pacienteModel = res;
            this.PacienteForm(this.pacienteModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  pacienteCreateForm: FormGroup;

  PacienteForm(paciente: PacienteModel = null) {
    this.pacienteCreateForm = this._fb.group({
      idPaciente: [paciente !== null ? paciente.idPaciente : 0],
      cedula: [paciente !== null ? paciente.cedula : "", Validators.required],
      nombres: [paciente !== null ? paciente.nombres : "", Validators.required],
      apellidos: [paciente !== null ? paciente.apellidos : "", Validators.required],
      telefono: [paciente !== null ? paciente.telefono : ""],
      sexo: [paciente !== null ? paciente.sexo : ""],
      usuario: [paciente !== null ? paciente.usuario : "", Validators.required],
      password: [paciente !== null ? paciente.password : "", Validators.required],
    });
  }

  Regresar(): void {
    this._router.navigate(["/arquitectura/list-paciente"]);
  }


  Guardar(): void {
    let nombrePaciente = this.pacienteCreateForm.get('nombres').value;
    let apellidoPaciente = this.pacienteCreateForm.get('apellidos').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `El Paciente ${nombrePaciente + ' ' + apellidoPaciente} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.pacienteModel = this.pacienteCreateForm.value;
          if (this.pacienteModel.idPaciente === 0) {
            if (this.pacienteCreateForm.valid) {
                    this._service.postPaciente(this.pacienteModel).subscribe((res) => {
                      console.log(res);
                        this.cargarPaciente();
                        swalWithBootstrapButtons.fire("Guardado!", `El Paciente ${nombrePaciente + ' ' + apellidoPaciente}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `El Paciente ${nombrePaciente + ' ' + apellidoPaciente}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }


  Modificar(): void {
    let nombrePaciente = this.pacienteCreateForm.get('nombres').value;
    let apellidoPaciente = this.pacienteCreateForm.get('apellidos').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Modificar!",
        text: `El Paciente ${nombrePaciente + ' ' + apellidoPaciente} se va a modificar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.pacienteModel = this.pacienteCreateForm.value;
            if (this.pacienteCreateForm.valid) {
              this._service.putPaciente(this.pacienteModel).subscribe((res) => {
                this.pacienteArray = this.pacienteArray.filter((paciente) => paciente !== this.pacienteModel);
                this.cargarPaciente();
                swalWithBootstrapButtons.fire(
                  "Modificado!",
                  `El Paciente ${nombrePaciente + ' ' + apellidoPaciente}, ha sido modificado`,
                  "success"
                );
                this.Regresar();
            },(error) => {
              swalWithBootstrapButtons.fire("No se pudo modificar!", `El Paciente ${nombrePaciente + ' ' + apellidoPaciente}`, "warning");
            });
        }else{
          this.Regresar();
        }

          }
      });
  }

}
