import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddEditCitasComponent } from '../arquitectura/citas/add-edit-citas/add-edit-citas.component';
import { ListCitasComponent } from '../arquitectura/citas/list-citas/list-citas.component';
import { AddEditEspecialidadComponent } from '../arquitectura/especialidad/add-edit-especialidad/add-edit-especialidad.component';
import { ListEspecialidadComponent } from '../arquitectura/especialidad/list-especialidad/list-especialidad.component';
import { LoginComponent } from '../arquitectura/login/login.component';
import { AddEditMedicoComponent } from '../arquitectura/medico/add-edit-medico/add-edit-medico.component';
import { DetalleFotoComponent } from '../arquitectura/medico/detalle-foto/detalle-foto.component';
import { ListMedicoComponent } from '../arquitectura/medico/list-medico/list-medico.component';
import { MedicoEspecialComponent } from '../arquitectura/medico/medico-especial/medico-especial.component';
import { AddEditPacienteComponent } from '../arquitectura/paciente/add-edit-paciente/add-edit-paciente.component';
import { ListPacienteComponent } from '../arquitectura/paciente/list-paciente/list-paciente.component';
import { InicioComponent } from './inicio/inicio.component';
import { NavbarComponent } from './navbar/navbar.component';

const routes: Routes = [

  {path: 'inicio', component: NavbarComponent},
  {path: 'list-especialidad', component: ListEspecialidadComponent},
  {path: 'add-edit-especialidad', component: AddEditEspecialidadComponent},
  {path: 'add-edit-especialidad/:idEspecialidad', component: AddEditEspecialidadComponent},
  {path: 'list-medico', component: ListMedicoComponent},
  {path: 'add-edit-medico', component: AddEditMedicoComponent},
  {path: 'add-edit-medico/:idMedico', component: AddEditMedicoComponent},
  {path: 'medico-especial', component: MedicoEspecialComponent},
  {path: 'medico-especial/:idMedico', component: MedicoEspecialComponent},
  {path: 'detalle-foto/:idMedico', component: DetalleFotoComponent},
  {path: 'list-paciente', component: ListPacienteComponent},
  {path: 'add-edit-paciente', component: AddEditPacienteComponent},
  {path: 'add-edit-paciente/:idPaciente', component: AddEditPacienteComponent},
  {path: 'list-citas', component: ListCitasComponent},
  {path: 'add-edit-citas', component: AddEditCitasComponent},
  {path: 'add-edit-citas/:id_cita', component: AddEditCitasComponent},

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ArquitecturaRoutingModule { }
