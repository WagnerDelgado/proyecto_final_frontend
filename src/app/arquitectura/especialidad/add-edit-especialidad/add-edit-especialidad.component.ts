import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { EspecialidadModel } from '../../models/especialidad';
import { EspecialidadService } from '../../services/especialidad.service';

@Component({
  selector: 'app-add-edit-especialidad',
  templateUrl: './add-edit-especialidad.component.html',
  styleUrls: ['./add-edit-especialidad.component.scss']
})
export class AddEditEspecialidadComponent implements OnInit {

  especialidadModel: EspecialidadModel = new EspecialidadModel();
  especialidadArray: EspecialidadModel []=[];

  constructor(private _router: Router, private _service: EspecialidadService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.EspecialidadForm();
  }

  ngOnInit(): void {
    this.cargarEspecialidad();
  }

  cargarEspecialidad(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["idEspecialidad"];
      if (id) {
        this._service.getEspecialidadById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.especialidadModel = res;
            this.EspecialidadForm(this.especialidadModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  especialidadCreateForm: FormGroup;

  EspecialidadForm(especialidad: EspecialidadModel = null) {
    this.especialidadCreateForm = this._fb.group({
      idEspecialidad: [especialidad !== null ? especialidad.idEspecialidad : 0],
      nombre: [especialidad !== null ? especialidad.nombre : "", Validators.required],
      descripcion: [especialidad !== null ? especialidad.descripcion : "", Validators.required],
    });
  }

  Regresar(): void {
    this._router.navigate(["/arquitectura/list-especialidad"]);
  }

  get NameNoValido() {
    return (
      this.especialidadCreateForm.get("nombre").invalid &&
      this.especialidadCreateForm.get("nombre").touched
    );
  }


  Guardar(): void {
    let especialidad = this.especialidadCreateForm.get('nombre').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `La especialidad ${especialidad} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.especialidadModel = this.especialidadCreateForm.value;
          if (this.especialidadModel.idEspecialidad === 0) {
            if (this.especialidadCreateForm.valid) {
                    this._service.postEspecialidad(this.especialidadModel).subscribe((res) => {
                      console.log(res);
                        this.cargarEspecialidad();
                        swalWithBootstrapButtons.fire("Guardado!", `La Especialidad ${this.especialidadModel.nombre}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `La Especialidad ${this.especialidadModel.nombre}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  Modificar(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Modificar!",
        text: "Seguro que desea modificar la especialidad",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.especialidadModel = this.especialidadCreateForm.value;
            if (this.especialidadCreateForm.valid) {
              this._service.putEspecialidad(this.especialidadModel).subscribe((res) => {
                this.especialidadArray = this.especialidadArray.filter((especialidad) => especialidad !== this.especialidadModel);
                this.cargarEspecialidad();
                swalWithBootstrapButtons.fire(
                  "Modificado!",
                  `La especialidad ${this.especialidadModel.nombre}, ha sido modificado`,
                  "success"
                );
                this.Regresar();
            },(error) => {
              swalWithBootstrapButtons.fire("No se pudo modificar!", `La especialidad ${this.especialidadModel.nombre}`, "warning");
            });
        }else{
          this.Regresar();
        }

          }
      });
  }

}
