import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditEspecialidadComponent } from './add-edit-especialidad.component';

describe('AddEditEspecialidadComponent', () => {
  let component: AddEditEspecialidadComponent;
  let fixture: ComponentFixture<AddEditEspecialidadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditEspecialidadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditEspecialidadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
