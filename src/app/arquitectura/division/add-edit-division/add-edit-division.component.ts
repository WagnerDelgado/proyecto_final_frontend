import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DivisionModel } from '../../models/division';
import { DivisionService } from '../../services/division.service';

@Component({
  selector: 'app-add-edit-division',
  templateUrl: './add-edit-division.component.html',
  styleUrls: ['./add-edit-division.component.scss']
})
export class AddEditDivisionComponent implements OnInit {

  divisionModel: DivisionModel = new DivisionModel();
  divisionArray: DivisionModel []=[];

  constructor(private _router: Router, private _service: DivisionService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.DivisionForm();
  }

  ngOnInit(): void {
    this.cargarDivision();
  }

  cargarDivision(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["id"];
      if (id) {
        this._service.getDivisionById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.divisionModel = res;
            this.DivisionForm(this.divisionModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  divisionCreateForm: FormGroup;

  DivisionForm(division: DivisionModel = null) {
    this.divisionCreateForm = this._fb.group({
      id: [division !== null ? division.id : 0],
      name: [division !== null ? division.name : "", Validators.required],
    });
  }

  Regresar(): void {
    this._router.navigate(["/list-division"]);
  }

  get NameNoValido() {
    return (
      this.divisionCreateForm.get("name").invalid &&
      this.divisionCreateForm.get("name").touched
    );
  }


  Guardar(): void {
    let division = this.divisionCreateForm.get('name').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `La division ${division} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.divisionModel = this.divisionCreateForm.value;
          if (this.divisionModel.id === 0) {
            if (this.divisionCreateForm.valid) {
                    this._service.postDivision(this.divisionModel).subscribe((res) => {
                      console.log(res);
                        this.cargarDivision();
                        swalWithBootstrapButtons.fire("Guardado!", `La Division ${this.divisionModel.name}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `La Division ${this.divisionModel.name}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  Modificar(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Modificar!",
        text: "Seguro que desea modificar la division",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.divisionModel = this.divisionCreateForm.value;
            if (this.divisionCreateForm.valid) {
              this._service.putCitas(this.divisionModel.id, this.divisionModel).subscribe((res) => {


                this.divisionArray = this.divisionArray.filter((division) => division !== this.divisionModel);
                this.cargarDivision();
                swalWithBootstrapButtons.fire(
                  "Modificado!",
                  `La Division ${this.divisionModel.name}, ha sido modificado`,
                  "success"
                );
                this.Regresar();
            },(error) => {
              swalWithBootstrapButtons.fire("No se pudo modificar!", `La Division ${this.divisionModel.name}`, "warning");
            });
        }else{
          this.Regresar();
        }

          }
      });
  }

}
