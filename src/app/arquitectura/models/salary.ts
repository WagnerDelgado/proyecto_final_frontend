import { DivisionModel } from "./division";
import { EmployeeModel } from "./Employee";
import { OfficeModel } from "./office";
import { PositionModel } from "./position";

export class SalaryModel {
  id: number;
  identificationNumber: string;
  employeeCode: string;
  employeeName: string;
  employeeSurname: string;
  officeId: number;
  office?: OfficeModel;
  divisionId: number;
  division?: DivisionModel;
  positionId: number;
  position?: PositionModel;
  grade: number;
  begin: Date;
  birthday: Date;
  year: number;
  month: number;
  baseSalary: number;
  employeeId: number;
  employee: EmployeeModel;
  productionBonus: number;
  compensationBonus: number;
  commission: number;
  contribution: number;
}


