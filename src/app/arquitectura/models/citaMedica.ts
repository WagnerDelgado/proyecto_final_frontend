import { MedicoModel } from "./medico";
import { PacienteModel } from "./paciente";

export class CitaMedicaModel{
  //idCita:number;
  id_cita:number;
  fecha_cita: Date;
  hora_cita: string;
  medico: MedicoModel;
  paciente: PacienteModel;
}
