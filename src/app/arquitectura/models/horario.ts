export class HorarioModel{
  idHorario: number;
  dias: string;
  hora_inicio: string;
  hora_fin: string;
}
