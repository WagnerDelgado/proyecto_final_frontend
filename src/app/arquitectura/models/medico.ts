import { EspecialidadModel } from "./especialidad";
import { HorarioModel } from "./horario";

export class MedicoModel{
  idMedico:number;
  cedula: string;
  nombres: string;
  apellidos: string;
  especialidad: EspecialidadModel;
  horario: HorarioModel;
}
