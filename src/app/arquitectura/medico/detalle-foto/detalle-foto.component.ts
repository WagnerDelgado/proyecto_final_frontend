import { HttpEventType } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import Swal from 'sweetalert2';
import { MedicoModel } from '../../models/medico';
import { MedicoService } from '../../services/medico.service';

@Component({
  selector: 'app-detalle-foto',
  templateUrl: './detalle-foto.component.html',
  styleUrls: ['./detalle-foto.component.scss']
})
export class DetalleFotoComponent implements OnInit {

  medicoModel: MedicoModel = new MedicoModel();
  fotoSeleccionada: File;
  progreso: number=0;


  constructor(private _medicoService: MedicoService, private _activatedRoute: ActivatedRoute) { }

  ngOnInit(): void {
    // this._activatedRoute.paramMap.subscribe(params =>{
    //   let id = +params.get('idMedico');
    //   if(id){
    //     this._medicoService.getMedicoById(id).subscribe(res =>{
    //         this.medicoModel = res;
    //     })
    //   }
    // })
  }

  seleccionarFoto(event){
    this.fotoSeleccionada = event.target.files[0];
    this.progreso = 0;
    console.log(this.fotoSeleccionada);
    if(this.fotoSeleccionada.type.indexOf('image') < 0){
      Swal.fire('Error al seleccionar la imagen!', `El archivo debe ser del tipo imagen`, 'error');
      this.fotoSeleccionada = null;
    }
  }

  // subirFoto(){
  //   if(!this.fotoSeleccionada){
  //     Swal.fire('Error al subir el archivo!', `Debe seleecionar una foto!`, 'error');
  //   }else{
  //     this._medicoService.subirImagen(this.fotoSeleccionada, this.medicoModel.idMedico).subscribe(event =>{

  //       if(event.type === HttpEventType.UploadProgress){
  //         this.progreso = Math.round((event.loaded/event.total)*100);
  //       }else if(event.type === HttpEventType.Response){
  //         let response: any = event.body;
  //         this.medicoModel = response.medico as MedicoModel;
  //         Swal.fire('Foto subida al sistema!', response.mensaje, 'success');
  //       }

  //     });
  //   }

  // }

}
