import { Component, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { MedicoModel } from '../../models/medico';
import { MedicoService } from '../../services/medico.service';
import { MedicoDialogoComponent } from '../medico-dialogo/medico-dialogo.component';

@Component({
  selector: 'app-list-medico',
  templateUrl: './list-medico.component.html',
  styleUrls: ['./list-medico.component.scss']
})
export class ListMedicoComponent implements OnInit {

  medicoModel: MedicoModel;
  medicoArray: MedicoModel[] = [];
  displayedColumns: string[] = ["idMedico", "cedula", "nombres", "apellidos", "id_especialidad", "id_horario_dia", "id_horario_desde", "id_horario_hasta", "acciones"];
  dataSource = new MatTableDataSource();
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private _router: Router, private _service: MedicoService, private dialog: MatDialog) {
    this.CargarTabla();
  }

  Buscar(filtrar: string): void {
    this.dataSource.filter = filtrar.trim().toLowerCase();
  }

  CargarTabla() {
    this.dataSource = new MatTableDataSource();
    this.isLoading = true;
    this._service.getMedico().subscribe(
      (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      },
      (error) => (this.isLoading = false)
    );
  }

  openDialog(medico?: MedicoModel){
    this.dialog.open(MedicoDialogoComponent, {
      width: '850px',
      height:'350px',
      data: medico
    })
  }


  ngOnInit(): void {}

  Eliminar(medico: MedicoModel): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Esta seguro?',
        text: 'Seguro que desea eliminar el Medico?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this._service.deleteMedico(medico.idMedico).subscribe(
            (res) => {

                this.medicoArray = this.medicoArray.filter((medico) => medico !== medico);
                this.CargarTabla();
                swalWithBootstrapButtons.fire(
                  'Eliminado!',
                  `El médico ha sido eliminado!`,
                  'success'
                );

            },
            (error) => {
              swalWithBootstrapButtons.fire(
                'No se pudo Eliminar!',
                `El médicoya se ha generado anteriormente!`,
                'warning'
              );
            }
          );
        }
      });
  }

}
