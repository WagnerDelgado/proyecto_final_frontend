import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable } from 'rxjs';
import Swal from 'sweetalert2';
import { EspecialidadModel } from '../../models/especialidad';
import { HorarioModel } from '../../models/horario';
import { MedicoModel } from '../../models/medico';
import { MedicoService } from '../../services/medico.service';

@Component({
  selector: 'app-medico-especial',
  templateUrl: './medico-especial.component.html',
  styleUrls: ['./medico-especial.component.scss']
})
export class MedicoEspecialComponent implements OnInit {

  constructor(private _service: MedicoService, private _router: Router, private _activatedRoute: ActivatedRoute, private _fb: FormBuilder) {
    this.MedicoForm();
  }

  form: FormGroup;
  medico: MedicoModel = new MedicoModel();
  medicoArray: MedicoModel[];
  especialidadArray: EspecialidadModel[];
  horarioArray: HorarioModel[];
  mycontrolEspecialidad: FormControl = new FormControl();
  mycontrolHorario: FormControl = new FormControl();
  especialidadSeleccionada: EspecialidadModel;


  cargarMedicos(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["idMedico"];
      if (id) {
        this._service.getMedicoById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.medico = res;
            this.MedicoForm(this.medico);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  MedicoForm(medico: MedicoModel = null) {
    this.form = this._fb.group({
      idMedico: [medico !== null ? medico.idMedico : 0],
      cedula: [medico !== null ? medico.cedula : "", Validators.required],
      nombres: [medico !== null ? medico.nombres : "", Validators.required],
      apellidos: [medico !== null ? medico.apellidos : "", Validators.required],
      especialidad: [medico !== null ? medico.especialidad : null],
      horario: [medico !== null ? medico.horario : null]
    });
  }


  ngOnInit(): void {

    this.cargarMedicos();
    // this.form = new FormGroup({
    //   idMedico: new FormControl(),
    //   cedula: new FormControl(),
    //   nombres: new FormControl(),
    //   apellidos: new FormControl(),
    //   fotoUrl: new FormControl(),
    //   especialidad: new FormControl(),
    //   horario: new FormControl(),
    // });

    this._service.getEspecialidad().subscribe((response) => {
      this.especialidadArray = response;
      console.log(response);
    });

    this._service.getHorario().subscribe((response) => {
      this.horarioArray = response;
      console.log(response);
    });


  }

  seleccionarEvento(e){
    console.log(e.value);
  }

  horarioSeleccionado(e){
    console.log(e.value);
  }

  Guardar(): void {
    let nombresMedico = this.form.get('nombres').value;
    let apellidosMedico = this.form.get('apellidos').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `El Médico ${nombresMedico + ' ' + apellidosMedico} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.medico = this.form.value;
          console.log(this.medico);
          if (this.medico.idMedico == 0) {
            if (this.form.valid) {
                    this._service.postMedico(this.medico).subscribe((res) => {
                      console.log(res);
                        this.cargarMedicos();
                        swalWithBootstrapButtons.fire("Guardado!", `El Médico ${nombresMedico + ' ' + apellidosMedico}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `El Médico ${nombresMedico + ' ' + apellidosMedico}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  Modificar(): void {

    let nombresMedico = this.form.get('nombres').value;
    let apellidosMedico = this.form.get('apellidos').value;

    const swalWithBootstrapButtons = Swal.mixin({
        customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
        },
        buttonsStyling: false,
    });

    swalWithBootstrapButtons
        .fire({
        title: "Modificar!",
        text: `Desea modificar el medico ${nombresMedico + ' ' + apellidosMedico}?`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
        })
        .then((result) => {
        if (result.isConfirmed) {
            this.medico = this.form.value;
            if (this.form.valid) {
                this._service.putMedico(this.medico).subscribe((res) => {
                if(res){
                //this.medicoArray = this.medicoArray.filter((medico) => medico !== this.medico);
                this.cargarMedicos();
                swalWithBootstrapButtons.fire(
                    "Modificado!",
                    `El medico ${nombresMedico + ' ' + apellidosMedico}, ha sido modificado`,
                    "success"
                );
                this.Regresar();
                }else{
                swalWithBootstrapButtons.fire(
                    "No se pudo Modificar!",
                    `El medico ${nombresMedico + ' ' + apellidosMedico}, ya existe y/o se encuentra vinculado!`,
                    "warning"
                );
                }
            });
        }else{
            this.Regresar();
        }

            }
        });
    }

  Regresar(): void {
    this._router.navigate(["/arquitectura/list-medico"]);
  }
}
