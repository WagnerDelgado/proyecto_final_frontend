import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { PositionModel } from '../../models/position';
import { PositionService } from '../../services/position.service';

@Component({
  selector: 'app-add-edit-position',
  templateUrl: './add-edit-position.component.html',
  styleUrls: ['./add-edit-position.component.scss']
})
export class AddEditPositionComponent implements OnInit {

  positionModel: PositionModel = new PositionModel();
  positionArray: PositionModel []=[];

  constructor(private _router: Router, private _service: PositionService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
    this.PositionForm();
  }

  ngOnInit(): void {
    this.cargarPosition();
  }

  cargarPosition(): void {
    this._activatedRoute.params.subscribe((params) => {
      let id = params["id"];
      if (id) {
        this._service.getPositionById(id).subscribe(
          (res) => {
            console.log("res: ", res);
            this.positionModel = res;
            this.PositionForm(this.positionModel);
          },
          (err) => {
            console.log("error: ", JSON.stringify(err));
          }
        );
      }
    });
  }

  positionCreateForm: FormGroup;

  PositionForm(position: PositionModel = null) {
    this.positionCreateForm = this._fb.group({
      id: [position !== null ? position.id : 0],
      name: [position !== null ? position.name : "", Validators.required],
    });
  }

  Regresar(): void {
    this._router.navigate(["/list-position"]);
  }

  get NameNoValido() {
    return (
      this.positionCreateForm.get("name").invalid &&
      this.positionCreateForm.get("name").touched
    );
  }


  Guardar(): void {
    let office = this.positionCreateForm.get('name').value;
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: "Agregar!",
        text: `La Office ${office} se va agregar al sistema!`,
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Guardar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.positionModel = this.positionCreateForm.value;
          if (this.positionModel.id === 0) {
            if (this.positionCreateForm.valid) {
                    this._service.postPosition(this.positionModel).subscribe((res) => {
                      console.log(res);
                        this.cargarPosition();
                        swalWithBootstrapButtons.fire("Guardado!", `La Position ${this.positionModel.name}, ha sido guardado`, "success");
                        this.Regresar();
                },(error) => {
                  swalWithBootstrapButtons.fire("No se pudo guardar!", `La Position ${this.positionModel.name}`, "warning");
                } );
          }
          }
        }else{
          this.Regresar();
        }
      });
  }

  Modificar(): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: "btn btn-success",
        cancelButton: "btn btn-danger",
      },
      buttonsStyling: false,
    });
    swalWithBootstrapButtons
      .fire({
        title: "Modificar!",
        text: "Seguro que desea modificar la position",
        icon: "warning",
        showCancelButton: true,
        confirmButtonText: "Si, Modificar",
        cancelButtonText: "No, Cancelar!",
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this.positionModel = this.positionCreateForm.value;
            if (this.positionCreateForm.valid) {
              this._service.putPosition(this.positionModel.id, this.positionModel).subscribe((res) => {
                this.positionArray = this.positionArray.filter((position) => position !== this.positionModel);
                this.cargarPosition();
                swalWithBootstrapButtons.fire(
                  "Modificado!",
                  `La Posicion ${this.positionModel.name}, ha sido modificado`,
                  "success"
                );
                this.Regresar();
            },(error) => {
              swalWithBootstrapButtons.fire("No se pudo modificar!", `La Division ${this.positionModel.name}`, "warning");
            });
        }else{
          this.Regresar();
        }

          }
      });
  }

}
