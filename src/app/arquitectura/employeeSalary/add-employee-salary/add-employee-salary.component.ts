import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { DivisionModel } from '../../models/division';
import { OfficeModel } from '../../models/office';
import { PositionModel } from '../../models/position';
import { SalaryModel } from '../../models/salary';
import { DivisionService } from '../../services/division.service';
import { OfficeService } from '../../services/office.service';
import { PositionService } from '../../services/position.service';
import { SalaryService } from '../../services/salary.service';

@Component({
  selector: 'app-add-employee-salary',
  templateUrl: './add-employee-salary.component.html',
  styleUrls: ['./add-employee-salary.component.scss']
})
export class AddEmployeeSalaryComponent implements OnInit {

 salaryModel: SalaryModel = new SalaryModel();
 salaryArray: SalaryModel[] = [];
 positionModel: PositionModel;
 positionArray: PositionModel[] = [];
 divisionModel: DivisionModel;
 divisionArray: DivisionModel[] = [];
 officeModel: OfficeModel;
 officeArray: OfficeModel[] = [];
 gradeList : number[] = [1,2,3,4,5,6,7,8,9,10];

 constructor(private _router: Router, private _serviceSalary: SalaryService, private _servicePosition: PositionService, private _serviceDivision: DivisionService, private _serviceOffice: OfficeService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
     this.EmployeeSalaryForm();
 }

 ngOnInit(): void {
     this.cargarEmployeesSalary();

     this._serviceOffice.getOffice().subscribe((response) => {
       this.officeModel = response;
   });

     this._servicePosition.getPosition().subscribe((response) => {
         this.positionModel = response;
     });

     this._serviceDivision.getDivision().subscribe((response) => {
         this.divisionModel = response;
     });
 }

 cargarEmployeesSalary(): void {
     this._activatedRoute.params.subscribe((params) => {
       let id = params["id"];
       if (id) {
         this._serviceSalary.getSalaryById(id).subscribe(
             (res) => {
               console.log("res:", res);
               this.salaryModel = res;
               this.EmployeeSalaryForm(this.salaryModel);
             },
             (err) => {
               console.log("error: ", JSON.stringify(err));
             }
           );
       }
     });
   }

 formGroupEmployeeSalary: FormGroup;

 EmployeeSalaryForm(salary: SalaryModel = null) {
     this.formGroupEmployeeSalary = this._fb.group({
         id: [salary != null ? salary.id : 0],
         identificationNumber: [salary != null ? salary.identificationNumber : ''],
         employeeCode: [salary != null ? salary.employeeCode : ''],
         employeeName: [salary != null ? salary.employeeName : ''],
         employeeSurname: [salary != null ? salary.employeeSurname : ''],
         officeId: [salary != null ? salary.officeId : ''],
         //office: [employee != null ? employee.office : ''],
         divisionId: [salary != null ? salary.divisionId : ''],
         //division: [employee != null ? employee.division : ''],
         positionId: [salary != null ? salary.positionId : ''],
         //position: [employee != null ? employee.position : ''],
         grade: [salary != null ? salary.grade : ''],
         begin: [salary !== null ? salary.begin : null, Validators.required],
         birthday: [salary != null ? salary.birthday : null, Validators.required],
         year: [salary != null ? salary.year : 0],
         month: [salary != null ? salary.month : 0],
         baseSalary: [salary != null ? salary.baseSalary : 0],
         productionBonus: [salary != null ? salary.productionBonus : 0],
         compensationBonus: [salary != null ? salary.compensationBonus : 0],
         commission: [salary != null ? salary.commission : 0],
         contribution: [salary != null ? salary.commission : 0],
     });
 }


 get identificationNoValido() {
  return (
      this.formGroupEmployeeSalary.get('identificationNumber').invalid &&
      this.formGroupEmployeeSalary.get('identificationNumber').touched
  );
}


 get codeNoValido() {
  return (
      this.formGroupEmployeeSalary.get('employeeCode').invalid &&
      this.formGroupEmployeeSalary.get('employeeCode').touched
  );
}

 get nameNoValido() {
     return (
         this.formGroupEmployeeSalary.get('employeeName').invalid &&
         this.formGroupEmployeeSalary.get('employeeName').touched
     );
 }

 get surnameNoValido() {
  return (
      this.formGroupEmployeeSalary.get('employeeSurname').invalid &&
      this.formGroupEmployeeSalary.get('employeeSurname').touched
  );
}

 get positionNoValido() {
     return (
         this.formGroupEmployeeSalary.get('positionId').invalid &&
         this.formGroupEmployeeSalary.get('positionId').touched
     );
 }

 get divisionNoValido() {
   return (
       this.formGroupEmployeeSalary.get('divisionId').invalid &&
       this.formGroupEmployeeSalary.get('divisionId').touched
   );
}

get officeNoValido() {
 return (
     this.formGroupEmployeeSalary.get('officeId').invalid &&
     this.formGroupEmployeeSalary.get('officeId').touched
 );
}

 Regresar(): void {
     this._router.navigate(['list-employee-salary']);
 }

 limpiar(): void {
     this.formGroupEmployeeSalary.reset();
 }

 ObtenerErrorCampoPosition() {
     var position = this.formGroupEmployeeSalary.get('positionId');
     if (position.hasError('required')) {
         return 'El campo position es requerido';
     }
     return '';
 }

 ObtenerErrorCampoDivision() {
     var division = this.formGroupEmployeeSalary.get('divisionId');
     if (division.hasError('required')) {
         return 'El campo division es requerido';
     }
     return '';
 }


 ObtenerErrorCampoOffice() {
   var office = this.formGroupEmployeeSalary.get('officeId');
   if (office.hasError('required')) {
       return 'El campo office es requerido';
   }
   return '';
}

//  Guardar(): void {
//      let nombres = this.formGroupEmployee.get('employeeName').value;
//      let apellidos = this.formGroupEmployee.get('employeeSurname').value;
//      const swalWithBootstrapButtons = Swal.mixin({
//          customClass: {
//              confirmButton: 'btn btn-success',
//              cancelButton: 'btn btn-danger',
//          },
//          buttonsStyling: false,
//      });

//      swalWithBootstrapButtons
//          .fire({
//              title: 'Agregar!',
//              text: `El empleado ${ nombres + ' ' + apellidos } se va agregar al sistema!`,
//              icon: 'warning',
//              showCancelButton: true,
//              confirmButtonText: 'Si, Guardar',
//              cancelButtonText: 'No, Cancelar!',
//              reverseButtons: true,
//          })
//          .then((result) => {
//              if (result.isConfirmed) {
//                  this.employeeModel = this.formGroupEmployee.value;
//                  console.log(this.employeeModel);
//                  this.employeeModel.id = 0;
//                  if (this.employeeModel.id == 0) {
//                      if (this.formGroupEmployee.valid) {
//                          this._serviceEmploye.postEmployee(this.employeeModel).subscribe(res => {
//                                      if (res !== null) {
//                                          swalWithBootstrapButtons.fire(
//                                              'Guardado!',
//                                              `El empleado ${this.employeeModel.employeeName +' ' + this.employeeModel.employeeSurname}, ha sido guardado`,
//                                              'success'
//                                          );
//                                          this.Regresar();
//                                          this.cargarEmployees();
//                                      } else {
//                                          swalWithBootstrapButtons.fire(
//                                              'No se pudo guardar!',
//                                              `El empleado ${ this.employeeModel.employeeName + ' ' + this.employeeModel.employeeSurname }, ya existe!`,
//                                              'warning'
//                                          );
//                                      }
//                                  },
//                                  (error) => {
//                                      //swalWithBootstrapButtons.fire("No se pudo guardar!", `debe activar al vendedor ${this.vendedor.vendedor}`, "warning");
//                                  }
//                              );
//                      }
//                  }
//              } else {
//                  this.Regresar();
//              }
//          });
//  }


Guardar(): void {
  let nombres = this.formGroupEmployeeSalary.get('employeeName').value;
  let apellidos = this.formGroupEmployeeSalary.get('employeeSurname').value;
  const swalWithBootstrapButtons = Swal.mixin({
    customClass: {
      confirmButton: "btn btn-success",
      cancelButton: "btn btn-danger",
    },
    buttonsStyling: false,
  });

  swalWithBootstrapButtons
    .fire({
      title: "Agregar!",
      text: `El empleado ${nombres + ' ' + apellidos} se va agregar al sistema!`,
      icon: "warning",
      showCancelButton: true,
      confirmButtonText: "Si, Guardar",
      cancelButtonText: "No, Cancelar!",
      reverseButtons: true,
    })
    .then((result) => {
      if (result.isConfirmed) {
        this.salaryModel = this.formGroupEmployeeSalary.value;
        if (this.salaryModel.id === 0) {
          if (this.formGroupEmployeeSalary.valid) {
                  this._serviceSalary.postSalary(this.salaryModel).subscribe((res) => {
                    console.log(res);
                      this.cargarEmployeesSalary();
                      swalWithBootstrapButtons.fire("Guardado!", `El empleado ${this.salaryModel.employeeName + ' ' + this.salaryModel.employeeSurname}, ha sido guardado`, "success");
                      this.Regresar();
              },(error) => {
                swalWithBootstrapButtons.fire("No se pudo guardar!", `El empleado ${this.salaryModel.employeeName + ' ' + this.salaryModel.employeeSurname}`, "warning");
              } );
        }
        }
      }else{
        this.Regresar();
      }
    });
}

// Modificar(): void {
//   let nombres = this.formGroupEmployeeSalary.get('employeeName').value;
//   let apellidos = this.formGroupEmployeeSalary.get('employeeSurname').value;
//   const swalWithBootstrapButtons = Swal.mixin({
//     customClass: {
//       confirmButton: "btn btn-success",
//       cancelButton: "btn btn-danger",
//     },
//     buttonsStyling: false,
//   });
//   swalWithBootstrapButtons
//     .fire({
//       title: "Modificar!",
//       text: `El empleado ${nombres + ' ' + apellidos} se va a modificar al sistema!`,
//       icon: "warning",
//       showCancelButton: true,
//       confirmButtonText: "Si, Modificar",
//       cancelButtonText: "No, Cancelar!",
//       reverseButtons: true,
//     })
//     .then((result) => {
//       if (result.isConfirmed) {
//         this.salaryArray = this.formGroupEmployeeSalary.value;
//           if (this.formGroupEmployeeSalary.valid) {
//             this._serviceSalary.put(this.employeeModel.id, this.employeeModel).subscribe((res) => {
//               this.employeeArray = this.employeeArray.filter((employee) => employee !== this.employeeModel);
//               this.cargarEmployees();
//               swalWithBootstrapButtons.fire(
//                 "Modificado!",
//                 `El emplado ${this.employeeModel.employeeName + ' ' + this.employeeModel.employeeSurname}, ha sido modificado`,
//                 "success"
//               );
//               this.Regresar();
//           },(error) => {
//             swalWithBootstrapButtons.fire("No se pudo modificar!", `El emplado ${this.employeeModel.employeeName + ' ' + this.employeeModel.employeeSurname}`, "warning");
//           });
//       }else{
//         this.Regresar();
//       }

//         }
//     });
// }

}
