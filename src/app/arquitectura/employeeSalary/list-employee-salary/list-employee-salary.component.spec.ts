import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ListEmployeeSalaryComponent } from './list-employee-salary.component';

describe('ListEmployeeSalaryComponent', () => {
  let component: ListEmployeeSalaryComponent;
  let fixture: ComponentFixture<ListEmployeeSalaryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ListEmployeeSalaryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ListEmployeeSalaryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
