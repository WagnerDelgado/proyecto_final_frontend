import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { SalaryModel } from '../models/salary';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class SalaryService {

  constructor(private _http: HttpClient) { }

  private url = environment.api + '/api/Salary';
  private urlPost = environment.api + '/api/Salary';
  private urlDelete = environment.api + '/api/Salary';

  getSalary(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getSalaryById(id: number): Observable<any> {
    return this._http.get(`${this.url}/${id}`, httpOptions);
  }

  postSalary(salaries: SalaryModel): Observable<any> {
    return this._http.post(this.urlPost, salaries, httpOptions);
  }

  // putEmployee(id: number, salary: SalaryModel[]): Observable<any> {
  //   return this._http.put(`${this.urlPut}/${id}`, employee, httpOptions);
  // }

  deleteSalary(id: number) {
    return this._http.delete(`${this.urlDelete}/${id}`, httpOptions);
  }


}
