import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Menu } from '../models/menu';

@Injectable({
  providedIn: 'root'
})
export class MenuService {

  // private menuCambio = new Subject<Menu[]>();

  // constructor(_http: HttpClient) {
  //   super(_http, `${environment.api}/menus`);
  // }

  // listarPorUsuario(nombre: string) {
  //   let token = localStorage.getItem(environment.TOKEN_NAME);

  //   return this._http.post<Menu[]>(`${this.url}/usuario`, nombre, {
  //     headers: new HttpHeaders().set('Authorization', `bearer ${token}`).set('Content-Type', 'application/json')
  //   });
  // }

  // getMenuCambio() {
  //   return this.menuCambio.asObservable();
  // }

  // setMenuCambio(menus: Menu[]) {
  //   this.menuCambio.next(menus);
  // }
}
