import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EmployeeModel } from '../models/Employee';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {

  constructor(private _http: HttpClient) { }

  private url = environment.api + '/api/Employee';
  private urlPost = environment.api + '/api/Employee';
  private urlPut = environment.api + '/api/Employee';
  private urlDelete = environment.api + '/api/Employee';


  getEmployee(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getEmployeeById(id: number): Observable<any> {
    return this._http.get(`${this.url}/${id}`, httpOptions);
  }

  postEmployee(employee: EmployeeModel): Observable<any> {
    return this._http.post(this.urlPost, employee, httpOptions);
  }

  putEmployee(id: number, employee: EmployeeModel): Observable<any> {
    return this._http.put(`${this.urlPut}/${id}`, employee, httpOptions);
  }

  deleteEmployee(id: number) {
    return this._http.delete(`${this.urlDelete}/${id}`, httpOptions);
  }
}
