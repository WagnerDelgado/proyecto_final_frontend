import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import Swal from 'sweetalert2';
import { CitaMedicaModel } from '../models/citaMedica';
import { MedicoModel } from '../models/medico';
import { PacienteModel } from '../models/paciente';


const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class CitaService {

  private url = environment.api + '/citas';
  private urlPost = environment.api + '/citas';
  private urlPut = environment.api + '/citas';
  private urlDelete = environment.api + '/citas';
  private urlMedico = environment.api + '/medicos';
  private urlPaciente = environment.api + '/pacientes';

  constructor(private _http: HttpClient) { }

  getMedicos(): Observable<MedicoModel[]> {
    return this._http.get<MedicoModel[]>(this.urlMedico, httpOptions);
  }

  getPacientes(): Observable<PacienteModel[]> {
    return this._http.get<PacienteModel[]>(this.urlPaciente, httpOptions);
  }

  getCitas(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getCitasById(idCita: number): Observable<CitaMedicaModel> {
    return this._http.get<CitaMedicaModel>(`${this.url}/${idCita}`, httpOptions);
  }

  // getCitasById(idCita: number): Observable<any> {
  //   return this._http.get(`${this.url}/${idCita}`).pipe(
  //     catchError((error) => {
  //       console.log(error.error.mensaje);
  //       Swal.fire('Error al buscar al cliente', error.error.mensaje, 'error');
  //       return throwError(error);
  //     })
  //   );
  // }

  postCitas(cita: CitaMedicaModel): Observable<any> {
    return this._http.post(this.urlPost, cita, httpOptions);
  }

  putCitas(cita: CitaMedicaModel): Observable<any> {
    return this._http.put(this.urlPut, cita, httpOptions);
  }

  deleteCitas(idCita: number) {
    return this._http.delete(`${this.urlDelete}/${idCita}`, httpOptions);
  }
}


