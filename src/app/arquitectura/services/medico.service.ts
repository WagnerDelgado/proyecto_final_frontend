import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EspecialidadModel } from '../models/especialidad';
import { HorarioModel } from '../models/horario';
import { MedicoModel } from '../models/medico';
import { GenericService } from './generic.service';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root',
})
export class MedicoService {
  // private medicoCambio = new Subject<MedicoModel[]>();
  // private mensajeCambio = new Subject<string>();

  // constructor(protected _http: HttpClient) {
  //   super(_http, `${environment.api}/medicos`);
  // }

  // setMensajeCambio(mensaje: string) {
  //   this.mensajeCambio.next(mensaje);
  // }

  // getMensajeCambio() {
  //   return this.mensajeCambio.asObservable();
  // }

  // setMedicoCambio(lista: MedicoModel[]) {
  //   this.medicoCambio.next(lista);
  // }

  // getMedicoCambio() {
  //   return this.medicoCambio.asObservable();
  // }

  constructor(private _http: HttpClient) { }

  private url = environment.api + '/medicos';
  private urlPost = environment.api + '/medicos';
  private urlPut = environment.api + '/medicos';
  private urlDelete = environment.api + '/medicos';
  private urlEspecialidad = environment.api + '/especialidades';
  private urlHorario = environment.api + '/horarios';

  getMedico(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getEspecialidad(): Observable<EspecialidadModel[]> {
    return this._http.get<EspecialidadModel[]>(this.urlEspecialidad, httpOptions);
  }

  getHorario(): Observable<HorarioModel[]> {
    return this._http.get<HorarioModel[]>(this.urlHorario, httpOptions);
  }

  getHorarios(): Observable<any> {
    return this._http.get(this.urlEspecialidad, httpOptions);
  }

  getMedicoById(idMedico: number): Observable<any> {
    return this._http.get(`${this.url}/${idMedico}`, httpOptions);
  }

  postMedico(medico: MedicoModel): Observable<any> {

    return this._http.post(this.urlPost, medico, httpOptions);
  }

  putMedico(medico: MedicoModel): Observable<any> {
    return this._http.put(`${this.urlPut}`, medico, httpOptions);
  }

  deleteMedico(idMedico: number) {
    return this._http.delete(`${this.urlDelete}/${idMedico}`, httpOptions);
  }

  // subirImagen(archivo:File, id): Observable<HttpEvent<{}>>{
  //   let formData = new FormData();
  //   formData.append("archivo", archivo);
  //   formData.append("id", id);
  //   const req = new HttpRequest('POST', `${this.urlPost}/upload`, formData);
  //   return this._http.request(req);
  // }
}
