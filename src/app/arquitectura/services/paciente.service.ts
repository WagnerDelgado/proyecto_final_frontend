import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PacienteModel } from '../models/paciente';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class PacienteService {

  private url = `${environment.api}/pacientes`;
  private urlPost = `${environment.api}/pacientes`;
  private urlPut = `${environment.api}/pacientes`;
  private urlDelete = `${environment.api}/pacientes`;

  constructor(private _http: HttpClient) { }

  getPaciente(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getPacienteById(idPaciente: number): Observable<any> {
    return this._http.get(`${this.url}/${idPaciente}`, httpOptions);
  }

  postPaciente(paciente: PacienteModel): Observable<any> {
    return this._http.post(this.urlPost, paciente, httpOptions);
  }

  putPaciente(paciente: PacienteModel): Observable<any> {
    return this._http.put(`${this.urlPut}`, paciente, httpOptions);
  }

  deletePaciente(idPaciente: number) {
    return this._http.delete(`${this.urlDelete}/${idPaciente}`, httpOptions);
  }
}
