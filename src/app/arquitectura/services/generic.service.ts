import { HttpClient } from '@angular/common/http';
import { Inject, Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class GenericService<T> {

  constructor(protected _http: HttpClient, @Inject("url") protected url: string) { }

  getAll(){
    return this._http.get<T[]>(`${this.url}`);
  }

  getById(id: number){
    return this._http.get<T[]>(`${this.url}/${id}`);
  }

  post(t: T){
    return this._http.post(this.url, t);
  }

  put(t: T){
    return this._http.post(this.url, t);
  }

  delete(id: number){
    return this._http.delete<T[]>(`${this.url}/${id}`);
  }
}
