import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';
import { DivisionModel } from '../models/division';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root',
})
export class DivisionService {
  constructor(private _http: HttpClient) {}

  private url = environment.api + '/api/Division';
  private urlPost = environment.api + '/api/Division';
  private urlPut = environment.api + '/api/Division';
  private urlDelete = environment.api + '/api/Division';

  getDivision(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getDivisionById(id: number): Observable<any> {
    return this._http.get(`${this.url}/${id}`, httpOptions);
  }

  postDivision(division: DivisionModel): Observable<any> {
    return this._http.post(this.urlPost, division, httpOptions);
  }

  putCitas(id: number, division: DivisionModel): Observable<any> {
    return this._http.put(`${this.urlPut}/${id}`, division, httpOptions);
  }

  deleteDivision(id: number) {
    return this._http.delete(`${this.urlDelete}/${id}`, httpOptions);
  }
}
