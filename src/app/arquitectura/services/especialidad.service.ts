import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { EspecialidadModel } from '../models/especialidad';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root'
})
export class EspecialidadService {

  constructor(private _http: HttpClient) { }

  private url = `${environment.api}/especialidades`;
  private urlPost = `${environment.api}/especialidades`;
  private urlPut = `${environment.api}/especialidades`;
  private urlDelete = `${environment.api}/especialidades`;

  getEspecialidad(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getEspecialidadById(idEspecialidad: number): Observable<any> {
    return this._http.get(`${this.url}/${idEspecialidad}`, httpOptions);
  }

  postEspecialidad(especialidad: EspecialidadModel): Observable<any> {
    return this._http.post(this.urlPost, especialidad, httpOptions);
  }

  putEspecialidad(especialidad: EspecialidadModel): Observable<any> {
    return this._http.put(`${this.urlPut}`, especialidad, httpOptions);
  }

  deleteEspecialidad(idEspecialidad: number) {
    return this._http.delete(`${this.urlDelete}/${idEspecialidad}`, httpOptions);
  }
}
