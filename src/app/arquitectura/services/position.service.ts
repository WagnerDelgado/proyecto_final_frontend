import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { PositionModel } from '../models/position';

const header = {
  'Content-Type': 'application/json',
};

const httpOptions = {
  headers: header,
};

@Injectable({
  providedIn: 'root',
})
export class PositionService {
  constructor(private _http: HttpClient) {}

  private url = environment.api + '/api/Position';
  private urlPost = environment.api + '/api/Position';
  private urlPut = environment.api + '/api/Position';
  private urlDelete = environment.api + '/api/Position';

  getPosition(): Observable<any> {
    return this._http.get(this.url, httpOptions);
  }

  getPositionById(id: number): Observable<any> {
    return this._http.get(`${this.url}/${id}`, httpOptions);
  }

  postPosition(position: PositionModel): Observable<any> {
    return this._http.post(this.urlPost, position, httpOptions);
  }

  putPosition(id: number, position: PositionModel): Observable<any> {
    return this._http.put(`${this.urlPut}/${id}`, position, httpOptions);
  }

  deletePosition(id: number) {
    return this._http.delete(`${this.urlDelete}/${id}`, httpOptions);
  }
}
