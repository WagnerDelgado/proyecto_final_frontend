import { Component, OnInit } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import Swal from 'sweetalert2';
import { EmployeeModel } from '../../models/Employee';
import { EmployeeService } from '../../services/employee.service';
import { SalaryService } from '../../services/salary.service';

@Component({
  selector: 'app-add-edit-salary',
  templateUrl: './add-edit-salary.component.html',
  styleUrls: ['./add-edit-salary.component.scss']
})
export class AddEditSalaryComponent implements OnInit {
  ngOnInit(): void {
    throw new Error('Method not implemented.');
  }

//   salaryModel: SalaryModel = new SalaryModel();
//   salaryListModel: SalaryListModel = new SalaryListModel();
//   employeeModel: EmployeeModel;
//   divFila1: boolean=true;
//   divFila2: boolean=false;
//   divFila3: boolean=false;
//   divFila4: boolean=false;
//   divFila5: boolean=false;
//   divFila6: boolean=false;
//   divFila7: boolean=false;
//   divFila8: boolean=false;
//   constructor(private _router: Router, private _serviceEmploye: EmployeeService, private _serviceSalary: SalaryService, private _fb: FormBuilder, private _activatedRoute: ActivatedRoute) {
//     this.SalaryForm();
//   }

//   ngOnInit(): void {
//     this._serviceEmploye.getEmployee().subscribe((response) => {
//       this.employeeModel = response;
//   });
//   }

//   formGroupSalary: FormGroup;

//   SalaryForm(salary: SalaryModel = null) {
//     this.formGroupSalary = this._fb.group({
//         id: [salary != null ? salary.id : 0],
//         // year: [salary != null ? salary.year : 0],
//         // month: [salary != null ? salary.month : 0],
//         // baseSalary: [salary != null ? salary.baseSalary : 0],
//         // employeeId: [salary != null ? salary.employeeId : ''],
//         // productionBonus: [salary != null ? salary.productionBonus : 0],
//         // compensationBonus: [salary != null ? salary.compensationBonus : 0],
//         // commission: [salary != null ? salary.commission : 0],
//         // contribution: [salary != null ? salary.commission : 0],
//         ListaSalaries: this._fb.array([this.ListaSalariesGroup(0,0,0,0,0,0,0,0,0)]),
//     });
// }



//   mostrarFila1(){
//     if(this.divFila1){
//       this.divFila2=true;
//     }
//   }

//   removerFila1(){
//     this.divFila1=false;
//   }

//   mostrarFila2(){
//     if(this.divFila2){
//       this.divFila3=true;
//     }
//   }

//   removerFila2(){
//     this.divFila2=false;
//   }

//   mostrarFila3(){
//     if(this.divFila3){
//       this.divFila4=true;
//     }
//   }

//   removerFila3(){
//     this.divFila3=false;
//   }

//   mostrarFila4(){
//     if(this.divFila4){
//       this.divFila5=true;
//     }
//   }

//   removerFila4(){
//     this.divFila4=false;
//   }

//   mostrarFila5(){
//     if(this.divFila5){
//       this.divFila6=true;
//     }
//   }

//   removerFila5(){
//     this.divFila5=false;
//   }


//   mostrarFila6(){
//     if(this.divFila6){
//       this.divFila7=true;
//     }
//   }

//   removerFila6(){
//     this.divFila6=false;
//   }

//   mostrarFila7(){
//     if(this.divFila7){
//       this.divFila8=true;
//     }
//   }

//   removerFila7(){
//     this.divFila7=false;
//   }

//   ListaSalariesGroup(id:number, year: number, month:number, baseSalary: number, employeeId: number, productionBonus: number, compensationBonus: number, commission: number, contribution:number) {
//     return this._fb.group({
//       id: [id],
//       year: [year],
//       month: [month],
//       baseSalary: [baseSalary],
//       employeeId: [employeeId],
//       productionBonus: [productionBonus],
//       compensationBonus: [compensationBonus],
//       commission: [commission],
//       contribution: [contribution]
//     });
//   }

//   get ListaSalaryArray(){
//     return <FormArray>this.formGroupSalary.get('ListaSalaries');
//   }

//   agregarListadoSalaries(){
//     this.ListaSalaryArray.push(this.ListaSalariesGroup(0,0,0,0,0,0,0,0,0));
//   }

//   removerListadoSalaries(index){
//       this.ListaSalaryArray.removeAt(index);
//   }

//   Regresar(): void {
//     this._router.navigate(['list-salary']);
// }


//   Guardar(){
//     let salarios: SalaryListModel[];
//     salarios = this.formGroupSalary.value.ListaSalaries;
//     console.log(salarios);

//     const swalWithBootstrapButtons = Swal.mixin({
//       customClass: {
//         confirmButton: "btn btn-success",
//         cancelButton: "btn btn-danger",
//       },
//       buttonsStyling: false,
//     });

//     swalWithBootstrapButtons
//       .fire({
//         title: "Agregar!",
//         text: `Se va agregar una lista de salarios al sistema!`,
//         icon: "warning",
//         showCancelButton: true,
//         confirmButtonText: "Si, Guardar",
//         cancelButtonText: "No, Cancelar!",
//         reverseButtons: true,
//       })
//       .then((result) => {
//         if (result.isConfirmed) {
//                     this._serviceSalary.postSalary(salarios).subscribe((res) => {
//                       console.log(res);
//                         //this.cargar();
//                         swalWithBootstrapButtons.fire("Guardado!", `El y/o los salarios, han sido guardados`, "success");
//                         this.Regresar();
//                 },(error) => {
//                   swalWithBootstrapButtons.fire("No se pudo guardar!", `El y/o la lista de salarios no se ha podido guardar`, "warning");
//                 } );

//         }else{
//           this.Regresar();
//         }
//       });
//   }
}
