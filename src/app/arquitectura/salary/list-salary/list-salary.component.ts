import { Component, OnInit, ViewChild } from '@angular/core';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { Router } from '@angular/router';
import Swal from 'sweetalert2';
import { SalaryModel } from '../../models/salary';
import { SalaryService } from '../../services/salary.service';

@Component({
  selector: 'app-list-salary',
  templateUrl: './list-salary.component.html',
  styleUrls: ['./list-salary.component.scss']
})
export class ListSalaryComponent implements OnInit {

  salaryModel: SalaryModel = new SalaryModel();
  salaryArray: SalaryModel[] = [];
  displayedColumns: string[] = ["employeeId", "year", "month", "productionBonus", "compensationBonus", "commission", "contribution", "acciones"];
  dataSource = new MatTableDataSource();
  isLoading = false;

  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort, { static: false }) sort: MatSort;

  constructor(private _router: Router, private _service: SalaryService) {
    this.CargarTabla();
  }

  Buscar(filtrar: string): void {
    this.dataSource.filter = filtrar.trim().toLowerCase();
  }

  CargarTabla() {
    this.dataSource = new MatTableDataSource();
    this.isLoading = true;
    this._service.getSalary().subscribe(
      (res: any) => {
        this.dataSource = new MatTableDataSource(res);
        this.dataSource.sort = this.sort;
        this.dataSource.paginator = this.paginator;
        this.isLoading = false;
      },
      (error) => (this.isLoading = false)
    );
  }

  ngOnInit(): void {

  }

  Eliminar(salary: SalaryModel): void {
    const swalWithBootstrapButtons = Swal.mixin({
      customClass: {
        confirmButton: 'btn btn-success',
        cancelButton: 'btn btn-danger',
      },
      buttonsStyling: false,
    });

    swalWithBootstrapButtons
      .fire({
        title: 'Esta seguro?',
        text: 'Seguro que desea eliminar el Salario?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonText: 'Si, eliminar',
        cancelButtonText: 'No, cancelar!',
        reverseButtons: true,
      })
      .then((result) => {
        if (result.isConfirmed) {
          this._service.deleteSalary(salary.id).subscribe(
            (res) => {
                this.salaryArray = this.salaryArray.filter((salary) => salary !== salary);
                this.CargarTabla();
                swalWithBootstrapButtons.fire(
                  'Eliminado!',
                  `El salario ha sido eliminado!`,
                  'success'
                );

            },
            (error) => {
              swalWithBootstrapButtons.fire(
                'No se pudo Eliminar!',
                `El salario ya se ha generado anteriormente!`,
                'warning'
              );
            }
          );
        }
      });
  }

}
