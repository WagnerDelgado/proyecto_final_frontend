import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'src/environments/environment';
import { LoginService } from '../services/login.service';
import '../../../assets/login-animation.js';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  usuario: string;
  clave: string;
  mensaje: string;
  error: string;

  constructor(private _router: Router, private _loginService: LoginService) { }


  ngOnInit(): void {
  }

  iniciarSesion(){
    this._loginService.login(this.usuario, this.clave).subscribe(res => {
      //console.log(res);
      localStorage.setItem(environment.TOKEN_NAME, res.access_token);
      this._router.navigate(['/arquitectura/inicio']);
    })
    // this.loginService.login(this.usuario, this.clave).subscribe(data=>{
    //   sessionStorage.setItem(environment.TOKEN_NAME, data.access_token);
    //   this.router.navigate(['pages/inicio']);
    // });
  }

  // ngAfterViewInit() {
  //   (window as any).initialize();
  // }

}
